<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

ini_set("memory_limit", -1);
ini_set('max_execution_time', -1);

function departamentos_existencia($departamentos,$departamento_codigo)
{	
	foreach ($departamentos as $departamento) {
		if($departamento['cod_depa']==$departamento_codigo)
			return true;
	}
	return false;
};

function provincia_existencia($provincias, $provincia_codigo)
{
	foreach ($provincias as $provincia) {
		if($provincia['cod_prov'] == $provincia_codigo)
			return true;
	}
	return false;
}

function distrito_existencia($distritos, $distrito_codigo)
{
	foreach ($distritos as $distrito) {
		if($distrito['cod_dist'] == $distrito_codigo)
			return true;
	}
	return false;
}

function ccpp_existencia($ccpps, $ccpp_codigo)
{
	foreach ($ccpps as $ccpp) {
		if($ccpp['cod_ccpp'] == $ccpp_codigo)
			return true;
	}
	return false;
}

function propiedad_array($departamentos, &$lista)
{
	foreach ($departamentos as $departamento) {
		$provincias = $departamento['provincias'];
		$provincias_lista = array();

		foreach ($provincias as $provincia) {			
			$distritos = $provincia['distritos'];
			$distritos_lista = array();

			foreach ($distritos as $distrito) {
				$ccpps = $distrito['ccpps'];
				$ccpps_lista = array();
				
				foreach ($ccpps as $ccpp) {
					//lista ccpp
					array_push($ccpps_lista, $ccpp);
				}
				$distrito['ccpps'] = $ccpps_lista;
				//lista dist
				array_push($distritos_lista, $distrito);
			}
			$provincia['distritos'] = $distritos_lista;

			//lista prov
			array_push($provincias_lista,$provincia);		
		}
		$departamento['provincias'] = $provincias_lista;

		//lista dep
		array_push($lista,$departamento);		
	}
	echo json_encode($lista);
	return;
}

Route::get('', function () {

	//echo date('l jS \of F Y h:i:s A');

	$lista = array();
	$departamentos = array();	
	$estado = true;
	$i = 0;

	Excel::load('ubigeo-test2.xlsx', function($reader) use($i,$estado,&$departamentos) {	

    	$reader->each(function($sheet) use($i,$estado,&$departamentos) { 
		    $sheet->each(function($row) use(&$i,$estado,&$departamentos) {
		    	//echo "<br>fila : ".$i."<br>";

		    	if(!isset($row->cod_depa))
		    		//echo "<br>fila depa: ".$i.$row."<br>";
		    	
		    	$departamento = null;
		    	$departamento = array();
		    	//crear departamento
		    	if(departamentos_existencia($departamentos, $row->cod_depa)==false)
		    	{
					$departamento['cod_depa'] = $row->cod_depa;
					$departamento['nomb_dpto'] = $row->nomb_dpto;
					$departamento['provincias'] = array();
					$departamentos[$row->cod_depa] = $departamento;
		    	}
		    	else
		    		$departamento = $departamentos[$row->cod_depa];

		    	if(!isset($row->cod_prov))
		    		echo "<br>fila prov: ".$i."<br>";
		    	
		    	$provincia = array();
		    	$provincias = $departamento['provincias'];
		    	
		    	//crear provincia
		    	if(provincia_existencia($provincias, $row->cod_prov)==false)
		    	{
		    		$provincia['cod_prov'] = $row->cod_prov;
					$provincia['nomb_prov'] = $row->nomb_prov;
					$provincia['distritos'] = array();
					$provincias[$row->cod_prov] = $provincia;

					$departamento['provincias'] = $provincias;
					$departamentos[$row->cod_depa] = $departamento;
				}
				else
					$provincia = $provincias[$row->cod_prov];

				if(!isset($row->cod_dist))
		    		echo "<br>fila dist: ".$i."<br>";

				$distrito = array();
		    	$distritos = $provincia['distritos'];

		    	//crear distrito
				if(distrito_existencia($distritos, $row->cod_dist)==false)
		    	{
		    		$distrito['cod_dist'] = $row->cod_dist;
		    		$distrito['nomb_dist'] = $row->nomb_dist;
		    		$distrito['iddist'] = $row->iddist;
		    		$distrito['ccpps'] = array();
		    		$distritos[$row->cod_dist] = $distrito;

		    		$provincia['distritos'] = $distritos;
		    		$provincias[$row->cod_prov] = $provincia;

		    		$departamento['provincias'] = $provincias;
					$departamentos[$row->cod_depa] = $departamento;
				}
				else
					$distrito = $distritos[$row->cod_dist];

				if(!isset($row->cod_ccpp))
		    		echo "<br>fila ccpp: ".$i."<br>";
		    	
		    		$ccpp = array();
					$ccpps = $distrito['ccpps'];

		    		//crear ccpp
			    	if(ccpp_existencia($ccpps, $row->cod_ccpp)==false)
			    	{
			    		$ccpp['cod_ccpp'] = $row->cod_ccpp;
			    		$ccpp['nomccpp_de'] = $row->nomccpp_de;
			    		$ccpp['idccpp'] = $row->idccpp;
			    		$ccpp['area'] = $row->area;
			    		$ccpps[$row->cod_ccpp] = $ccpp;

			    		$distrito['ccpps'] = $ccpps;
			    		$distritos[$row->cod_dist] = $distrito;

			    		$provincia['distritos'] = $distritos;
			    		$provincias[$row->cod_prov] = $provincia;

			    		$departamento['provincias'] = $provincias;
						$departamentos[$row->cod_depa] = $departamento;
			    	}		    			    	
		    		
		    	//echo "<br>fila : ".$i."<br>";
		    	$i++;
		    });

		});

	});

	

	//echo "<br>".date('l jS \of F Y h:i:s A')."<br>";
	//echo json_encode($departamentos);
	//print_r($departamentos);

	//return Response::json($departamentos);
	$algo = array("a" => 2,3,4,5);
	$algos = array();
	$algos[] = $algo;
	//echo json_encode($algos);
	//echo json_encode($departamentos);
	//Response::json($algos);

	propiedad_array($departamentos, $lista);	
});
